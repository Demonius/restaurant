package com.here_food.restorator.api

import com.here_food.restorator.BuildConfig
import com.here_food.restorator.RestoratorApplication
import com.here_food.restorator.util.AppSharedUser
import com.here_food.restorator.util.SecureText
import dmitriy.lykashenko.by.food.Api.IApiService
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.ArrayList
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext

open class ApiService {
    private val cacheSize: Long = 100 * 1024 * 1024 // 100mb

    val cashe = Cache(RestoratorApplication.instance.cacheDir, cacheSize)

    private val specs = ArrayList<ConnectionSpec>()

    private val secureText = SecureText(RestoratorApplication.instance.applicationContext)

    //    private val certificatePinner = CertificatePinner.Builder()
    private val certificatePinner = CertificatePinner.Builder()
            .add(BuildConfig.SERT_URL, "sha256/nKWcsYrc+y5I8vLf1VGByjbt+Hnasjl+9h8lNKJytoE=")
            .add(BuildConfig.SERT_URL, "sha256/r/mIkG3eEpVdm+u/ko/cwxzOMo1bk4TyHIlByibiA5E=")
            .build()

    private val okHttpBuilder = OkHttpClient.Builder()
            .followRedirects(true)
            .followSslRedirects(true)
            .retryOnConnectionFailure(true)
            .addInterceptor { chain ->
                val accessToken = secureText.decryptText(AppSharedUser.accessToken ?: "")
                val stringClient = AppSharedUser.client ?: ""
                val client = secureText.decryptText(stringClient)
                val expiry = AppSharedUser.expiry
                val uid = secureText.decryptText(AppSharedUser.uidid ?: "")
                val request = chain.request()
                        .newBuilder()
                        .addHeader("access-token", accessToken)
                        .addHeader("token-type", "Bearer")
                        .addHeader("client", client)
                        .addHeader("expiry", "$expiry")
                        .addHeader("uid", uid)
                        .build()
                chain.proceed(request)
            }
            .cache(cashe)
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .hostnameVerifier { _, _ -> true }

    private val connectionSpec: ConnectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
            .tlsVersions(TlsVersion.TLS_1_2)
            .build()
    var okHttpClient: OkHttpClient

    init {
        val sslContext = SSLContext.getInstance("TLSv1.2")
        sslContext.init(null, null, null)
        sslContext.createSSLEngine()
        specs.add(connectionSpec)
        specs.add(ConnectionSpec.COMPATIBLE_TLS)
        specs.add(ConnectionSpec.CLEARTEXT)

        okHttpClient = okHttpBuilder.connectionSpecs(specs)
                .certificatePinner(certificatePinner)
                .build()
    }

    private val retrofit = Retrofit.Builder()
            .client(okHttpClient)
//            .baseUrl(ConstantParameters().url_api_ssl)
            .baseUrl(BuildConfig.API_BASE_URL_SSL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    protected val service: IApiService = retrofit.create(IApiService::class.java)


}