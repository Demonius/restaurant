package com.here_food.restorator.api.response

import com.here_food.restorator.ui.fragment.orders.models.Order

data class ResponseGetOrder(
    val data: Order

)