package com.here_food.restorator.api

import com.here_food.restorator.RestoratorApplication
import com.here_food.restorator.api.request.RequestChangeStatus
import com.here_food.restorator.api.response.ResponseGetOrder
import com.here_food.restorator.api.response.ResponseOrderList
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.util.AppSharedUser
import com.here_food.restorator.util.SecureText
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response

object ApiServiceContent : ApiService() {

    private val secureText = SecureText(RestoratorApplication.instance.applicationContext)

    fun getOrders(callback: (Observable<Response<ResponseOrderList>>) -> Unit) {
        callback(service.getOrders(AppSharedUser.locale!!))
    }

    fun getOrders(state: Int, callback: (Observable<Response<ResponseOrderList>>) -> Unit) {
        callback(service.getOrders(AppSharedUser.locale!!, state))
    }

    fun changeStatus(changeStatus: RequestChangeStatus, callback: (Observable<Response<ResponseBody>>) -> Unit) {
        callback(service.patchStatusOrder(changeStatus, AppSharedUser.locale!!))
    }

    fun getOrder(id: Long): Observable<Response<ResponseGetOrder>> =
        service.getOrder(id,AppSharedUser.locale!!)



//    fun logOut(callback: (Observable<Response<Unit>>) -> Unit) {
//        callback(service.logOut(AppShared.localeSetting!!))
//    }

//    fun singIn(user: SignInModel, callback: (Observable<Response<ResponseSignInModel>>) -> Unit) {
//        callback(service.signIn(user, AppSharedUser.locale!!))
//    }

//    fun signUp(user: SignUpModel, callback: (Observable<Response<ResponseSignUpModel>>) -> Unit) {
//        callback(service.signUp(user, AppShared.localeSetting!!)
//        )
//    }

//    fun resetPassword(password: String, confirm_password: String, callback: (Observable<Response<ResponseSignInModel>>) -> Unit) {
//callback(service.changePassword(RequestChangePassword(password,confirm_password), AppShared.localeSetting!!))
//    }


}

data class StateOrders(
    val idState: Int,
    val orders: ArrayList<Order>
)