package com.here_food.restorator.api.response

import com.here_food.restorator.ui.fragment.orders.models.Order
import java.io.Serializable


data class ResponseSignInModel(
    val data: DataLogin?,
    val roles: List<Role>,
    val restaurant: Order.Restaurant

) {
    data class DataLogin(
        val id: Long,
        val email: String,
        val provider: String,
        val name: String,
        val tel: Long,
        val uid: String,
        val avatar: Avatar,
        val locale: String,
        val noty: String,
        val brand_id: Int,
        val restaurant_id: Int,
        val birthday: String?
    )

    data class Avatar(
        val url: String,
        val normal: Logo
    ) {
        data class Logo(val url: String) : Serializable
    }

    data class Role(
        val id: Long,
        val name: String
    )
}
