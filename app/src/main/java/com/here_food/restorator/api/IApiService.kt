package dmitriy.lykashenko.by.food.Api

import com.here_food.restorator.api.request.RequestChangeStatus
import com.here_food.restorator.api.request.SignInModel
import com.here_food.restorator.api.response.ResponseGetOrder
import com.here_food.restorator.api.response.ResponseOrderList
import com.here_food.restorator.api.response.ResponseSignInModel
import com.here_food.restorator.ui.fragment.orders.models.Order
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface IApiService {

//    @POST("{locale}/api/${ConstantParameters.VERSION_API}/auth/")
//    fun signUp(
//            @Body user: SignUpModel,
//            @Path("locale") localeSetting: String
//    ): Observable<Response<ResponseSignUpModel>>

    // V1

    @POST("{locale}/api/v1/auth/sign_in/")
    fun signIn(
        @Body user: SignInModel,
        @Path("locale") localeSetting: String
    ): Observable<Response<ResponseSignInModel>>

    @GET("{locale}/api/v2/admin_app/restaurant_members/orders?page=1&per_page=50")
    fun getOrders(@Path("locale") locale: String): Observable<Response<ResponseOrderList>>

    @GET("{locale}/api/v2/admin_app/restaurant_members/orders")
    fun getOrders(
        @Path("locale") locale: String,
        @Query("status") status: Int,
        @Query("page") page: Long = 1,
        @Query("per_page") per_page: Int = 100
    ): Observable<Response<ResponseOrderList>>

    @PATCH("{locale}/api/v2/admin_app/restaurant_members/orders")
    fun patchStatusOrder(
        @Body changeStatus: RequestChangeStatus,
        @Path("locale") locale: String
    ): Observable<Response<ResponseBody>>

    @GET("{locale}/api/v2/admin_app/restaurant_members/orders/{idOrder}")
    fun getOrder(@Path("idOrder") id: Long, @Path("locale") locale: String): Observable<Response<ResponseGetOrder>>
}