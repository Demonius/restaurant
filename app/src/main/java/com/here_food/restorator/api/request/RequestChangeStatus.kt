package com.here_food.restorator.api.request

data class RequestChangeStatus(
    val order_id:Long,
    val new_status:Int
)