package com.here_food.restorator.api.request

import com.google.gson.annotations.SerializedName

data class SignInModel(
        @SerializedName("login") val tel: String,
        @SerializedName("password") val password: String
)