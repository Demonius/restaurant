package com.here_food.restorator.api.response

import com.here_food.restorator.ui.fragment.orders.models.Order

data class ResponseOrderList(
    val data: List<Order>?,
    val meta: Meta
) {
    data class Meta(
        val current_page: Long,
        val next_page: Long?,
        val prev_page: Long?,
        val total_pages: Long,
        val total_count: Long
    )
}