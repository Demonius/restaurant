package com.here_food.restorator.api

import com.here_food.restorator.RestoratorApplication
import com.here_food.restorator.api.request.SignInModel
import com.here_food.restorator.api.response.ResponseSignInModel
import com.here_food.restorator.util.AppSharedUser
import com.here_food.restorator.util.SecureText
import io.reactivex.Observable
import retrofit2.Response

object ApiServiceUser : ApiService() {

    private val secureText = SecureText(RestoratorApplication.instance.applicationContext)


//    fun logOut(callback: (Observable<Response<Unit>>) -> Unit) {
//        callback(service.logOut(AppShared.localeSetting!!))
//    }

    fun singIn(user: SignInModel, callback: (Observable<Response<ResponseSignInModel>>) -> Unit) {
        callback(service.signIn(user, AppSharedUser.locale!!))
    }

//    fun signUp(user: SignUpModel, callback: (Observable<Response<ResponseSignUpModel>>) -> Unit) {
//        callback(service.signUp(user, AppShared.localeSetting!!)
//        )
//    }

//    fun resetPassword(password: String, confirm_password: String, callback: (Observable<Response<ResponseSignInModel>>) -> Unit) {
//callback(service.changePassword(RequestChangePassword(password,confirm_password), AppShared.localeSetting!!))
//    }


}