package com.here_food.restorator.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import com.here_food.restorator.BuildConfig
import com.here_food.restorator.R
import com.here_food.restorator.RestoratorApplication
import com.here_food.restorator.api.ApiService
import com.here_food.restorator.service.websocket.Subscriber
import com.here_food.restorator.ui.fragment.orders.*
import com.here_food.restorator.ui.main.MainActivity
import com.here_food.restorator.util.AppSharedUser
import com.here_food.restorator.util.Util

class ServiceWebSocket : Service() {

    private lateinit var ex: ExecutorService
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate() {
        super.onCreate()
        ex = Executors.newFixedThreadPool(2)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val pendingIntent = intent!!.getParcelableExtra<PendingIntent>("pendingIntent")
        val run = StartObservableService(this, 555, pendingIntent, compositeDisposable)
        ex.submit(run)
        return START_REDELIVER_INTENT
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    class StartObservableService(
        private val context: ServiceWebSocket,
        private val startId: Int,
        private val pendingIntent: PendingIntent,
        private val compositeDisposable: CompositeDisposable
    ) : Runnable {


        override fun run() {
            val okHttpBuilder = OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .addInterceptor { chain ->
                    val request = chain.request()
                        .newBuilder()
                        .addHeader("origin", "android")
                        .build()
                    chain.proceed(request)
                }
                .cache(ApiService().cashe)
                .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                .hostnameVerifier { _, _ -> true }

            OrdersFragment.okHttpClient = okHttpBuilder.build()
            val url = BuildConfig.API_BASE_URL_WSS + "/?access-token=${RestoratorApplication.secureText.decryptText(
                AppSharedUser.accessToken
                    ?: ""
            )}&" +
                    "client=${RestoratorApplication.secureText.decryptText(AppSharedUser.client ?: "")}&" +
                    "uid=${RestoratorApplication.secureText.decryptText(AppSharedUser.email ?: "")}&" +
                    "origin=android"
            OrdersFragment.scarlet = Scarlet.Builder()
                .webSocketFactory(OrdersFragment.okHttpClient.newWebSocketFactory(url))
                .addMessageAdapterFactory(GsonMessageAdapter.Factory())
                .addStreamAdapterFactory(RxJava2StreamAdapterFactory())
                .build()

            OrdersFragment.connect = OrdersFragment.scarlet.create()


            val observableConnection = OrdersFragment.connect.observableOnOnConnectionOpen()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        is WebSocket.Event.OnConnectionOpened<*> -> {
                            Log.d(Util.LOG_TAG, "Connection opened")

                            val appChannel =
                                Subscriber(
                                    command = Subscriber.Type.SUBSCRIBE.command,
                                    identifier = Gson().toJson(
                                        Subscriber.Identifier(
                                            Subscriber.Channels.APPCHANNEL.channel,
                                            "admin_app_${AppSharedUser.restaurantId}"
                                        )
                                    )
                                )
                            OrdersFragment.connect.sendSubscriber(appChannel)
                        }
                        is WebSocket.Event.OnConnectionClosing -> Log.d(Util.LOG_TAG, "Connection closing")
                        is WebSocket.Event.OnConnectionClosed -> Log.d(Util.LOG_TAG, "Connection closed")
                        is WebSocket.Event.OnConnectionFailed -> Log.e(Util.LOG_TAG, "Connection failed", it.throwable)
                        is WebSocket.Event.OnMessageReceived -> {
                            analyseMessageFromServer(it.message.toString(), pendingIntent, context, startId)

                        }
                        else -> Log.d(Util.LOG_TAG, it.toString())
                    }
                }

            val observableMessage = OrdersFragment.connect.observeMessage().subscribe { message ->
                //                Log.d(Util.LOG_TAG, "message from server => $message")
            }
            compositeDisposable.addAll(observableConnection, observableMessage)
        }

        private fun analyseMessageFromServer(
            message: String,
            pendingIntent: PendingIntent,
            context: ServiceWebSocket,
            startId: Int
        ) {
            Log.d(Util.LOG_TAG, "message raw => $message")

            val index = message.indexOf("=")
            val text = message.substring(index + 1, message.length - 1)
            val json = Gson().fromJson(text, MessageWebSocket::class.java)
//            Log.d(Util.LOG_TAG, "message => $text")


            when (json.type) {
                "confirm_subscription" -> {
                    val intent = Intent().putExtra("message", context.resources.getString(R.string.enter_to_service))
                        .putExtra("status", 1)
                    pendingIntent.send(context, startId, intent)
                }
                null -> {
                    val orderMessage = Gson().fromJson(text, MessageFromWebSocket::class.java)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        val name = context.getString(R.string.channel_name)
                        val descriptionText = ""
                        val importance = NotificationManager.IMPORTANCE_HIGH
                        val mChannel = NotificationChannel("999", name, importance)
                        mChannel.description = descriptionText
                        // Register the channel with the system; you can't change the importance
                        // or other notification behaviors after this
                        val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                        notificationManager.createNotificationChannel(mChannel)
                    }

                        when (orderMessage.message.response) {
                        "NEW" -> {
                            val order = Gson().fromJson(orderMessage.message.order, DataOrder::class.java).data

                            val openIntent = Intent(context, MainActivity::class.java).apply {
                                putExtra(MainActivity.OPEN, true)
                                putExtra(MainActivity.ORDER, order)
                            }

                            val intent = Intent(StateOrdersFragment.BROADCAST_ACTION)
                            intent.putExtra(StateOrdersFragment.DATA, false)
                            intent.putExtra(StateOrdersFragment.ORDER, order.id)
                            intent.putExtra(StateOrdersFragment.STATUS, order.status)
                            intent.putExtra(StateOrdersFragment.ORDER_DATA, order)
                            context.sendBroadcast(intent)

                            val applyIntent = Intent(context, MainActivity::class.java).apply {
                                putExtra(MainActivity.NOTIFICATION_ID, order.id)
                                putExtra(MainActivity.STATUS_ID, 2)
                                putExtra(MainActivity.ORDER, order)
                            }

                            val cancelIntent = Intent(context, MainActivity::class.java).apply {
                                putExtra(MainActivity.NOTIFICATION_ID, order.id)
                                putExtra(MainActivity.STATUS_ID, 3)
                                putExtra(MainActivity.ORDER, order)
                            }

                            val openOrderIntent = PendingIntent.getActivity(context, 111, openIntent, 0)
                            val applyPendingIntent = PendingIntent.getActivity(context, 222, applyIntent, 0)
                            val cancelPendingIntent = PendingIntent.getActivity(context, 333, cancelIntent, 0)

//                            val smallTextMessage = StringBuilder()
//                                .append(context.resources.getString(R.string.type_in_order))
//                                .append(" ")
//                                .append(context.resources.getString(if (order.reservation) R.string.reservation else R.string.delivery))
//                                .toString()

                            val bigTextMessage = StringBuilder()
                                .append(context.resources.getString(R.string.type_in_order))
                                .append(" ")
                                .append(context.resources.getString(if (order.reservation) R.string.reservation else R.string.delivery))
                                .append("\n")
                                .append(context.resources.getString(R.string.notify_date_time_order))
                                .append(" ")
                                .append(Util.convertTimeDateNotify(order.order_date ?: "00:00 00/00/0000"))
                                .append("\n")
                                .append(context.resources.getString(if (order.reservation) R.string.person_in_order else R.string.address_in_order))
                                .append(" ")
                                .append(if (order.reservation) order.persons_quantity else order.address)
                                .append("\n")
                                .append(context.resources.getString(R.string.comment_in_order))
                                .append(" ")
                                .append(if (order.comment.isEmpty()) context.resources.getString(R.string.empty_comment) else order.comment)
                                .toString()


                            val notifyBuilder = NotificationCompat.Builder(context, "999")
                                .setSmallIcon(R.drawable.icon_notification_small)
                                .setContentTitle(context.resources.getString(R.string.app_name))
                                .setContentText(context.resources.getString(R.string.title_notify_new_order))
                                .setContentIntent(openOrderIntent)
                                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.app_icon))
                                .setStyle(
                                    NotificationCompat.BigTextStyle()
                                        .bigText(bigTextMessage)

                                )
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                                .setVibrate(longArrayOf(500, 500, 500, 500, 500))
                                .setLights(Color.RED, 500, 500)
                                .addAction(
                                    R.drawable.ic_tick,
                                    context.resources.getString(R.string.text_button_apply_order),
                                    applyPendingIntent
                                )
                                .addAction(
                                    R.drawable.ic_cancel,
                                    context.resources.getString(R.string.text_button_cancel_order),
                                    cancelPendingIntent
                                )
//                                .setAutoCancel(true)
                            NotificationManagerCompat.from(context)
                                .notify(order.id.toInt(), notifyBuilder.build())
                        }
                        "STATUS_CHANGED" -> {
                            val statusChanged = Gson().fromJson(orderMessage.message.order, DataOrder::class.java)
                            val oldStatus = orderMessage.message.old_status
                            val newStatus = orderMessage.message.new_status
                            val order = statusChanged.data

                            val openIntent = Intent(context, MainActivity::class.java).apply {
                                putExtra(MainActivity.OPEN, true)
                                putExtra(MainActivity.ORDER, order)
                            }
                            val intent = Intent(StateOrdersFragment.BROADCAST_ACTION)
                            intent.putExtra(StateOrdersFragment.DATA, true)
                            intent.putExtra(StateOrdersFragment.ORDER, order.id)
                            intent.putExtra(StateOrdersFragment.STATUS_NEW, newStatus)
                            intent.putExtra(StateOrdersFragment.STATUS_OLD, oldStatus)
                            intent.putExtra(StateOrdersFragment.ORDER_DATA, order)
                            context.sendBroadcast(intent)
                            NotificationManagerCompat.from(context).cancel(order.id.toInt())

                            val arrayStatus = context.resources.getStringArray(R.array.name_state)
                            val color = ContextCompat.getColor(
                                context, when (Util.convertStatusStringInInt(newStatus!!)) {
                                    1 -> R.color.colorStateNew
                                    2 -> R.color.colorStateInProgress
                                    3 -> R.color.colorStateCancel
                                    4 -> R.color.colorStateCompleted
                                    else -> R.color.colorStateDelivery
                                }
                            )

                            val openOrderIntent = PendingIntent.getActivity(context, 444, openIntent, 0)

                            val notifyBuilder = NotificationCompat.Builder(context, "999")
                                .setSmallIcon(R.drawable.icon_notification_small)
                                .setContentTitle(context.resources.getString(R.string.app_name))
                                .setContentText(context.resources.getString(R.string.notify_title_change_status))
                                .setContentIntent(openOrderIntent)
                                .setStyle(
                                    NotificationCompat.BigTextStyle()
                                        .bigText(
                                            String.format(
                                                context.resources.getString(R.string.notify_text_change_status),
                                                order.order_num,
                                                arrayStatus[Util.convertStatusStringInInt(newStatus) - 1]
                                            )
                                        )
                                )
                                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.app_icon))
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                                .setVibrate(longArrayOf(500, 500, 500, 500, 500))
                                .setLights(Color.RED, 500, 500)
                                .setColor(color)
                                .setAutoCancel(true)
                            NotificationManagerCompat.from(context)
                                .notify(order.id.toInt(), notifyBuilder.build())
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}
