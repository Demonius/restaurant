package com.here_food.restorator.service.websocket

import com.google.gson.annotations.SerializedName
import com.tinder.scarlet.Message
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Flowable


interface WorkSubscriber {
    @Send
    fun sendSubscriber(subscriber: Subscriber)

    @Send
    fun sendCoordinate(coordinate: Coordinate)

    @Receive
    fun observableOnOnConnectionOpen(): Flowable<WebSocket.Event>

    @Receive
    fun observeMessage(): Flowable<Message>
}

data class Subscriber(
        @SerializedName("command") val command: String,
        @SerializedName("identifier") val identifier: String

) {

    data class Identifier(
            @SerializedName("channel") val channel: String,
            @SerializedName("room") val room:String
    )

    enum class Type(val command: String) {
        SUBSCRIBE("subscribe"),
        UNSUBSCRIBE("unsubscribe"),
        MESSAGE("message")

    }

    enum class Channels(val channel: String) {
        APPCHANNEL("AdminAppChannel")
    }
}

data class Coordinate(
        @SerializedName("command") val command: String,
        @SerializedName("identifier") val identifier: String,
        @SerializedName("data") val data: String
) {
    data class Data(
            @SerializedName("action") val action: String,
            @SerializedName("lat") val lat: Double,
            @SerializedName("lon") val lon: Double
    )
}
