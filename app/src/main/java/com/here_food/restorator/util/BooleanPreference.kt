package com.here_food.restorator.util


import kotlin.reflect.KProperty

class BooleanPreference(override val type: Type) : StoredPreferencesUser<Boolean>(type) {
    override operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean {
        return pref.getBoolean(type.name, true)
    }

    override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
        pref.edit().putBoolean(type.name, value).apply()
    }
}