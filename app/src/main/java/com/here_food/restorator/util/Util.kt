package com.here_food.restorator.util

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.text.SpannableString
import android.text.style.StyleSpan
import com.google.android.material.textfield.TextInputLayout
import com.here_food.restorator.R
import org.jetbrains.anko.progressDialog
import java.text.SimpleDateFormat
import java.util.*

object Util {

    const val LOG_TAG = "RESTORATOR"
    private var dialog: ProgressDialog? = null
    const val formatDateTime = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    private val arrayInt = listOf(1, 2, 3, 4, 5)
    private val arrayString = listOf("NEW", "INPROGRESS", "CANCELED", "COMPLETED", "DELIVERY")

    fun checkedLogin(str: String, inputLayout: TextInputLayout): Boolean {

        return if (android.util.Patterns.EMAIL_ADDRESS.matcher(str).matches()) {
            inputLayout.isErrorEnabled = false
            inputLayout.error = ""
            true
        } else {
            if (android.util.Patterns.PHONE.matcher(str).matches() && str.length > 7) {
                inputLayout.isErrorEnabled = false
                inputLayout.error = ""
                true
            } else {
                inputLayout.error = inputLayout.context.getString(R.string.error_login)
                inputLayout.isErrorEnabled = true
                false
            }
        }
    }

    fun checkedPassword(password: String, inputLayout: TextInputLayout): Boolean {
        return if (password.length >= 6) {
            inputLayout.error = ""
            inputLayout.isErrorEnabled = false
            true
        } else {
            inputLayout.error = inputLayout.context.getString(R.string.error_password)
            inputLayout.isErrorEnabled = true
            false
        }
    }

    fun showLoadDialog(context: Context, message: String) {
        dialog = context.progressDialog(message) {
            isIndeterminate = true
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }
        dialog?.show()

    }

    fun closeDialogLoad() {
        if (dialog != null) dialog?.dismiss()
    }

    fun getTime(dateTime: String): String {
        val date = SimpleDateFormat(formatDateTime, Locale.getDefault()).parse(dateTime)
        return SimpleDateFormat("HH:mm", Locale.getDefault()).format(date)
    }

    fun getDate(dateTime: String): String {
        val date = SimpleDateFormat(formatDateTime, Locale.getDefault()).parse(dateTime)
        return SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(date)
    }

    fun convertStatusStringInInt(status: String): Int =
        arrayInt[arrayString.map { it.toLowerCase() }.indexOf(status.toLowerCase())]

    fun convertStatusIntToString(status: Int): String = arrayString[arrayInt.indexOf(status)]

    fun styledTextWithTitle(title: String, text: String): SpannableString {
        val spannableString = SpannableString("$title $text")
        spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, title.length, 0)
        return spannableString
    }

    fun convertTimeDate(dateTime: String): String {
        val date = SimpleDateFormat(formatDateTime, Locale.getDefault()).parse(dateTime)
        return SimpleDateFormat("dd/MM/yyyy \nHH:mm", Locale.getDefault()).format(date)
    }

    fun convertTimeDateNotify(dateTime: String): String {
        val date = SimpleDateFormat(formatDateTime, Locale.getDefault()).parse(dateTime)
        return SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.getDefault()).format(date)
    }

    fun convertFormatToDate(order_date: String?): String {
        val date = SimpleDateFormat(formatDateTime, Locale.getDefault()).parse(order_date)
        return if (order_date == null) "00/00/0000" else SimpleDateFormat(
            "dd/MM/yyyy",
            Locale.getDefault()
        ).format(date)
    }

    fun convertFormatToTime(order_date: String?): String {
        val date = SimpleDateFormat(formatDateTime, Locale.getDefault()).parse(order_date)
        return if (order_date == null) "00:00" else SimpleDateFormat("HH:mm", Locale.getDefault()).format(date)
    }
}