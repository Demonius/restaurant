package com.here_food.restorator.util

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Build
import se.simbio.encryption.Encryption

class SecureText constructor(context: Context) {
    private var key:String = try{
        (context.applicationContext.getSystemService(Context.WIFI_SERVICE)as WifiManager).connectionInfo.macAddress
    }catch (error:IllegalStateException){
        "111111"
    }
    var salt:String = Build.BOOTLOADER  //text about bootloader

    val size:Int = 16

    private val encryption: Encryption = Encryption.getDefault(key, salt, kotlin.ByteArray(size))

    fun encryptText(text:String): String{
        return encryption.encrypt(text)
    }

    fun decryptText(text: String):String{
        return encryption.decrypt(text)
    }




}