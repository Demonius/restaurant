package com.here_food.restorator.util

import kotlin.reflect.KProperty

class FloatPreference(override val type: Type) : StoredPreferencesUser<Float>(type)
{
    override operator fun getValue(thisRef: Any?, property: KProperty<*>): Float
    {
        return pref.getFloat(type.name, -1f)
    }

    override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Float)
    {
        pref.edit().putFloat(type.name, value).apply()
    }
}