package com.here_food.restorator.util

import android.content.Context
import android.content.SharedPreferences
import com.here_food.restorator.RestoratorApplication
import kotlin.reflect.KProperty

abstract class StoredPreferencesUser<T>(protected open val type: Type)
{
    enum class Type
    {
        AccessToken,
        Client,
        Expiry,
        Email,
        Name,
        Uid,
        Tel,
        Password,
        Avatar,
        UID,
        Locale,
        RestaurantName,
        RestaurantId,
        ServiceDelivery
    }

    protected val pref: SharedPreferences by lazy {
        RestoratorApplication.instance.getSharedPreferences("user", Context.MODE_PRIVATE)
    }

    abstract operator fun getValue(thisRef: Any?, property: KProperty<*>): T

    abstract operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T)
}