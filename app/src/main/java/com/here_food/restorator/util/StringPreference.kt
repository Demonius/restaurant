package com.here_food.restorator.util

import kotlin.reflect.KProperty

class StringPreference(override val type: Type) : StoredPreferencesUser<String?>(type)
{
    override operator fun getValue(thisRef: Any?, property: KProperty<*>): String?
    {
        return pref.getString(type.name, null)
    }

    override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String?)
    {
        pref.edit().putString(type.name, value).apply()
    }
}