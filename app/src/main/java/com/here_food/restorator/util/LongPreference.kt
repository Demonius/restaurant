package com.here_food.restorator.util

import kotlin.reflect.KProperty

class LongPreference(override val type: Type) : StoredPreferencesUser<Long>(type)
{
    override operator fun getValue(thisRef: Any?, property: KProperty<*>): Long
    {
        return pref.getLong(type.name, -1)
    }

    override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long)
    {
        pref.edit().putLong(type.name, value).apply()
    }
}