package com.here_food.restorator.util

object AppSharedUser {
    val accessToken by StringPreference(StoredPreferencesUser.Type.AccessToken)
    val client by StringPreference(StoredPreferencesUser.Type.Client)
    val expiry by LongPreference(StoredPreferencesUser.Type.Expiry)
    val email by StringPreference(StoredPreferencesUser.Type.Email)
    val name by StringPreference(StoredPreferencesUser.Type.Name)
    val uid by LongPreference(StoredPreferencesUser.Type.Uid)
    val tel by StringPreference(StoredPreferencesUser.Type.Tel)
    val password by StringPreference(StoredPreferencesUser.Type.Password)
    val avatar by StringPreference(StoredPreferencesUser.Type.Avatar)
    val uidid by StringPreference(StoredPreferencesUser.Type.UID)
    val locale by StringPreference(StoredPreferencesUser.Type.Locale)
    val restaurantName by StringPreference(StoredPreferencesUser.Type.RestaurantName)
    val restaurantId by LongPreference(StoredPreferencesUser.Type.RestaurantId)
    val serviceDelivery by BooleanPreference(StoredPreferencesUser.Type.ServiceDelivery)
}