package com.here_food.restorator.ui.fragment.auth.binding

import android.widget.Button
import androidx.databinding.BindingAdapter

@BindingAdapter("setStateLogin", "setStatePassword", requireAll = true)
fun setStateButton(button: Button, stateLogin: Boolean, statePassword: Boolean) {
    button.isEnabled = stateLogin && statePassword
}