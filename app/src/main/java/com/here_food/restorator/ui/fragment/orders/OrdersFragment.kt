package com.here_food.restorator.ui.fragment.orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.here_food.restorator.R
import com.here_food.restorator.databinding.OrdersFragmentBinding
import com.here_food.restorator.service.websocket.WorkSubscriber
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.ui.fragment.orders.view_model.OrdersViewModel
import com.tinder.scarlet.Scarlet
import kotlinx.android.synthetic.main.orders_fragment.*
import okhttp3.OkHttpClient

class OrdersFragment : Fragment() {

    companion object {
        lateinit var okHttpClient: OkHttpClient
        lateinit var scarlet: Scarlet
        lateinit var connect: WorkSubscriber
        const val ORDER = "order"

        fun newInstance() = OrdersFragment()

    }

    private lateinit var binding: OrdersFragmentBinding
    private lateinit var interfaceOrdersFragment: InterfaceOrdersFragment
    fun registrationInterface(interfaceOrdersFragment: InterfaceOrdersFragment) {
        this.interfaceOrdersFragment = interfaceOrdersFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.orders_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val order = arguments?.getParcelable<Order>(ORDER)

        binding.ordersView = OrdersViewModel(this)
        binding.ordersView?.loadTabs()
        binding.ordersView?.order?.set(order)
        binding.handler = this
        binding.ordersView?.menu?.set(R.menu.menu_toolbar)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (childFragmentManager.fragments.isNotEmpty())
            childFragmentManager.fragments.forEach {
                it.onRequestPermissionsResult(
                    requestCode,
                    permissions,
                    grantResults
                )
            }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    fun logout() {
        interfaceOrdersFragment.logout()
    }
}

data class MessageWebSocket(
    val identifier: String?,
    val type: String?
)

data class MessageFromWebSocket(
    val identifier: String,
    val message: MessageOrder
) {
    data class MessageOrder(
        val response: String,
        val order: String,
        val order_id: Long?,
        val old_status: String?,
        val new_status: String?
    )

}

data class DataOrder(val data: Order)

interface InterfaceOrdersFragment {
    fun logout()
}