package com.here_food.restorator.ui.fragment.orders.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.here_food.restorator.R
import com.here_food.restorator.ui.fragment.orders.InterfaceFragmentState
import com.here_food.restorator.ui.fragment.orders.StateOrdersFragment
import com.here_food.restorator.ui.fragment.orders.models.Order

class AdapterTabsOrder(
    context: Context,
    childFragmentManager: FragmentManager,
    private val listener: InterfaceFragmentState,
    private val order: Order?
) :
    FragmentStatePagerAdapter(childFragmentManager) {
    private val arrayStatus = listOf(1, 2, 5, 4, 3)
    private val titles = context.resources.getStringArray(R.array.name_state)

    override fun getItem(position: Int): Fragment {
        val fragment = StateOrdersFragment.newInstance(arrayStatus[position], order)
        fragment.registrationInterface(listener)
        return fragment
    }


    override fun getCount(): Int =
        titles.count()

    override fun getPageTitle(position: Int): CharSequence? = titles[position]


//    override fun getItemPosition(`object`: Any): Int {+
////        val fragment = getItem(arrayStatus.indexOf(Util.convertStatusStringInInt(orderUpdate?.status.toString())))
//        return when {
//            `object` is StateOrdersFragment && orderUpdate != null &&
//                    `object`.arguments!!["state"] as Int == Util.convertStatusStringInInt(
//                orderUpdate!!.status
//            ) -> {
//                `object`.updateOrder()
//                val statusInt = Util.convertStatusStringInInt(orderUpdate?.status.toString())
//                orderUpdate = null
//                arrayStatus.indexOf(statusInt)
//
//            }
//            else -> FragmentStatePagerAdapter.POSITION_UNCHANGED
//
//        }
//
//    }

//    fun updateOrder(order: Order) {
//        orderUpdate = order
//        notifyDataSetChanged()
//    }
}