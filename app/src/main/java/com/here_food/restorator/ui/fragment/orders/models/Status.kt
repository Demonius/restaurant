package com.here_food.restorator.ui.fragment.orders.models

data class Status(
    val id: Int,
    val name: String,
    val state:Boolean
)