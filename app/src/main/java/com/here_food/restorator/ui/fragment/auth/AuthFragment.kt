package com.here_food.restorator.ui.fragment.auth

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil

import com.here_food.restorator.R
import com.here_food.restorator.databinding.AuthFragmentBinding
import com.here_food.restorator.ui.fragment.auth.view_model.AuthViewModel
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert

class AuthFragment : Fragment() {

    companion object {
        fun newInstance() = AuthFragment()
    }


    private lateinit var interfaceOpenOrdersList: InterfaceAuthFragment

    private lateinit var viewModel: AuthViewModel
    private lateinit var binding: AuthFragmentBinding

    fun registrationInterface(interfaceOpenOrdersList: InterfaceAuthFragment) {
        this.interfaceOpenOrdersList = interfaceOpenOrdersList
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.auth_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.authView = AuthViewModel(this)
    }

    fun openOrdersFragment() {
        interfaceOpenOrdersList.openOrdersList()
    }

    fun hideKeyboard(view:View) {
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        imm.hideSoftInputFromWindow(view.windowToken,InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun showMessageForUser(message: String) {
        alert(message) { okButton { } }.show()
    }
}

interface InterfaceAuthFragment {
    fun openOrdersList()
}
