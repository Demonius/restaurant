package com.here_food.restorator.ui.fragment.auth.view_model

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import com.here_food.restorator.R
import com.here_food.restorator.RestoratorApplication
import com.here_food.restorator.api.ApiServiceUser
import com.here_food.restorator.api.request.SignInModel
import com.here_food.restorator.ui.fragment.auth.AuthFragment
import com.here_food.restorator.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.auth_fragment.*

class AuthViewModel(private val fragment: AuthFragment) {

    var accessToken by StringPreference(StoredPreferencesUser.Type.AccessToken)
    var client by StringPreference(StoredPreferencesUser.Type.Client)
    var expiry by LongPreference(StoredPreferencesUser.Type.Expiry)
    var email by StringPreference(StoredPreferencesUser.Type.Email)
    var name by StringPreference(StoredPreferencesUser.Type.Name)
    var uid by StringPreference(StoredPreferencesUser.Type.UID)
    var avatar by StringPreference(StoredPreferencesUser.Type.Avatar)
    var restaurantName by StringPreference(StoredPreferencesUser.Type.RestaurantName)
    var serviceDelivery by BooleanPreference(StoredPreferencesUser.Type.ServiceDelivery)
    var restaurantId by LongPreference(StoredPreferencesUser.Type.RestaurantId)

    //    val title = ObservableField<String>()
    val hintEmailInput = ObservableField<String>()
    val hintPasswordInput = ObservableField<String>()
    //    val textSignUpButton = ObservableField<String>()
    //    val textLostPassword = ObservableField<String>()
    val errorLogin = ObservableField<Boolean>()
    val errorPassword = ObservableField<Boolean>()
    private var textLogin = ""
    private var textPassword = ""

    init {
        hintEmailInput.set(fragment.context!!.getString(R.string.sing_in_name))
        hintPasswordInput.set(fragment.context!!.getString(R.string.sing_in_password))
//        textLostPassword.set("Забыли пароль")
//        textSignUpButton.set(view.resources.getString(R.string.button_sign_up))
        errorLogin.set(false)
        errorPassword.set(false)
    }

    fun onLoginTextChange(login: CharSequence) {
        textLogin = login.toString()
        errorLogin.set(Util.checkedLogin(login.toString(), fragment.inputLayoutName))
    }

    fun onPasswordTextChange(password: CharSequence) {
        textPassword = password.toString()
        errorPassword.set(Util.checkedPassword(password.toString(), fragment.inputLayoutPassword))
    }



    fun onLoginClick(view: View) {
        fragment.hideKeyboard(view)
        Util.showLoadDialog(fragment.context!!, fragment.context!!.getString(R.string.please_wait))

        ApiServiceUser.singIn(SignInModel(textLogin.replace("+",""), textPassword)) { observable ->
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Util.closeDialogLoad()
                    when (response.code()) {

                        200 -> {
                            val user = response.body()!!
                            val roles = user.roles
                            if ("restaurant" in roles.map { it.name }) {
                                val headers = response.headers()
                                accessToken =
                                        RestoratorApplication.secureText.encryptText(headers["access-token"] as String)
                                client = RestoratorApplication.secureText.encryptText(headers["client"] as String)
                                expiry = headers["expiry"]!!.toLong()
                                uid = RestoratorApplication.secureText.encryptText(headers["uid"] as String)
                                email = RestoratorApplication.secureText.encryptText(user.data?.email.toString())
                                name = user.data?.name
                                avatar = user.data?.avatar?.url
                                restaurantName = user.restaurant.name
                                restaurantId = user.restaurant.id
                                serviceDelivery = user.restaurant.delivery_service
                                fragment.openOrdersFragment()
                            } else {
                                fragment.showMessageForUser(fragment.context!!.getString(R.string.error_permission_signin))
                            }
                        }
                        401 -> fragment.showMessageForUser(response.errorBody()!!.string())

                    }
                }, { error ->
                    Util.closeDialogLoad()
                    Log.e(Util.LOG_TAG, "error sign in -> $error")
                })
        }
    }
}
