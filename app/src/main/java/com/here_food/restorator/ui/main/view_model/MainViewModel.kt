package com.here_food.restorator.ui.main.view_model

import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import com.here_food.restorator.ui.fragment.auth.AuthFragment
import com.here_food.restorator.ui.fragment.auth.InterfaceAuthFragment
import com.here_food.restorator.ui.fragment.orders.InterfaceOrdersFragment
import com.here_food.restorator.ui.fragment.orders.OrdersFragment
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.ui.main.MainActivity
import com.here_food.restorator.util.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class MainViewModel(val activity: MainActivity) {
    var accessToken by StringPreference(StoredPreferencesUser.Type.AccessToken)
    var client by StringPreference(StoredPreferencesUser.Type.Client)
    var expiry by LongPreference(StoredPreferencesUser.Type.Expiry)
    var email by StringPreference(StoredPreferencesUser.Type.Email)
    var name by StringPreference(StoredPreferencesUser.Type.Name)
    var uid by StringPreference(StoredPreferencesUser.Type.UID)
    var avatar by StringPreference(StoredPreferencesUser.Type.Avatar)
    var restaurantName by StringPreference(StoredPreferencesUser.Type.RestaurantName)
    var serviceDelivery by BooleanPreference(StoredPreferencesUser.Type.ServiceDelivery)
    var restaurantId by LongPreference(StoredPreferencesUser.Type.RestaurantId)
    var locale by StringPreference(StoredPreferencesUser.Type.Locale)
    val fragment = ObservableField<Fragment>()
    val order = ObservableField<Order>()

    init {
        if (locale == null) locale = "ru"

        if (AppSharedUser.uidid == null || AppSharedUser.uidid == "")

            setAuthFragment()
        else
            setOrdersFragment()

    }

    fun setOrdersFragment() {
        activity.connectToWebServer()
        val ordersFragment = OrdersFragment.newInstance()
        ordersFragment.registrationInterface(object : InterfaceOrdersFragment {
            override fun logout() {
                accessToken = null
                client = null
                expiry = -1
                email = null
                name = null
                uid = null
                avatar = null
                restaurantName = null
                serviceDelivery = false
                restaurantId = -1L
                activity.closeConnectionWebSocket()
                setAuthFragment()
            }

        })
        fragment.set(ordersFragment)
    }

    private fun setAuthFragment() {
        val authFragment = AuthFragment.newInstance()
        authFragment.registrationInterface(object : InterfaceAuthFragment {
            override fun openOrdersList() {
                setOrdersFragment()
            }

        })
        fragment.set(authFragment)
    }
}