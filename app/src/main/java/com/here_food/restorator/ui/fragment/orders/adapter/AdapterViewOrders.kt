package com.here_food.restorator.ui.fragment.orders.adapter

import com.here_food.restorator.BR
import com.here_food.restorator.R
import com.here_food.restorator.core.BaseRecyclerViewAdapter
import com.here_food.restorator.ui.fragment.orders.models.Order

class AdapterViewOrders: BaseRecyclerViewAdapter<Order>(R.layout.orders_item_view,BR.order)