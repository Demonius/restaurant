package com.here_food.restorator.ui.fragment.orders.binding

import android.graphics.Typeface
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import cn.refactor.multistatelayout.MultiStateLayout
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.tabs.TabLayout
import com.google.maps.android.SphericalUtil
import com.here_food.restorator.R
import com.here_food.restorator.core.BaseRecyclerViewAdapter
import com.here_food.restorator.ui.fragment.orders.InterfaceFragmentState
import com.here_food.restorator.ui.fragment.orders.adapter.AdapterTabsOrder
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.ui.fragment.orders.models.Status
import com.here_food.restorator.ui.fragment.orders.view_model.OrdersViewModel
import com.here_food.restorator.util.AppSharedUser
import com.here_food.restorator.util.Util
import org.jetbrains.anko.*
import java.lang.StringBuilder

@BindingAdapter("order:setNum")
fun setText(view: TextView, number: String) {
    view.text = Util.styledTextWithTitle(view.context.getString(R.string.number_order), number)
}

@BindingAdapter("order:setTime")
fun setTextTime(view: TextView, dateTime: String?) {
    val time = if (dateTime != null) Util.getTime(dateTime) else "00:00"
    view.text = Util.styledTextWithTitle(view.context.getString(R.string.time_in_order), time)
}

@BindingAdapter("order:setDate")
fun setTextDate(view: TextView, dateTime: String?) {
    val date = if (dateTime != null) Util.getDate(dateTime) else "00/00/0000"
    view.text = Util.styledTextWithTitle(view.context.getString(R.string.date_in_order), date)
}

@BindingAdapter("order:setType")
fun setTextType(view: TextView, state: Boolean) {
    val text = "Тип:"
    val spannableString = SpannableString("$text ${if (state) "Бронирование" else "Доставка"}")
    spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, text.length, 0)
    view.text = Util.styledTextWithTitle(
        view.context.getString(R.string.type_in_order),
        if (state) view.context.getString(R.string.booking_table) else view.context.getString(R.string.delivery_food)
    )
}

@BindingAdapter("order:setAdditionalInfo", "order:setReservation", requireAll = true)
fun setTextAdditional(view: TextView, element: String, state: Boolean) {
    view.text = Util.styledTextWithTitle(
        if (state) view.context.getString(R.string.person_in_order) else view.context.getString(R.string.address_in_order),
        element
    )
}


@BindingAdapter("setAdapter")
fun setAdapter(view: RecyclerView, adapter: BaseRecyclerViewAdapter<Order>) {
    val layoutManager = LinearLayoutManager(view.context)
    view.layoutManager = layoutManager
    view.adapter = adapter
}


@BindingAdapter("bind:handler", "bind:pager", "bind:listener", "bind:update", requireAll = true)
fun bindViewPagerAdapter(
    view: ViewPager,
    fragment: Fragment,
    tabLayout: TabLayout,
    listener: InterfaceFragmentState,
    order: Order?
) {
    val adapter = AdapterTabsOrder(view.context, fragment.childFragmentManager, listener, order)
    view.adapter = adapter
    tabLayout.setupWithViewPager(view, true)
    view.offscreenPageLimit = 5

    val arrayStatus = listOf(1, 2, 5, 4, 3)
    val arrayNameStatus = view.context.resources.getStringArray(R.array.name_state)
    val arrayIconTabs = listOf(
        R.drawable.ic_blank_file,
        R.drawable.ic_time_left,
        R.drawable.ic_box,
        R.drawable.ic_tick,
        R.drawable.ic_cancel
    )

    (0 until tabLayout.tabCount).forEach { position ->
        val tab = tabLayout.getTabAt(position)

        val tabChild = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(position)
        tabChild.requestLayout()

        val viewTab = LayoutInflater.from(view.context).inflate(R.layout.tab_layout_view, null)
        viewTab.findViewById<TextView>(R.id.name).text = arrayNameStatus[arrayStatus[position] - 1]
        viewTab.findViewById<TextView>(R.id.name).visibility = if (position == 0) View.VISIBLE else View.GONE
        viewTab.findViewById<ImageView>(R.id.image).imageResource = arrayIconTabs[position]
        tab?.customView = viewTab

    }
    view.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        }

        override fun onPageSelected(position: Int) {

            (0 until tabLayout.tabCount).forEach { idTab ->
                val tab = tabLayout.getTabAt(idTab)
                tab?.customView?.findViewById<TextView>(R.id.name)?.visibility =
                        if (idTab == position) View.VISIBLE else View.GONE
            }
        }
    })
//    if (order != null)
//        (0 until view.adapter!!.count).forEach {
//            ((view.adapter as AdapterTabsOrder).getItem(it) as StateOrdersFragment).openOrder(order)
//        }

}

@BindingAdapter("onClickBack")
fun onClickBack(view: Toolbar, onClickListener: View.OnClickListener) {
    view.setNavigationOnClickListener(onClickListener)
}

@BindingAdapter("setState")
fun setState(view: MultiStateLayout, state: Int) {
    view.state = state
}

@BindingAdapter("setLoadingChange", "textError", requireAll = true)
fun setLoading(view: ImageView, state: OrdersViewModel.StateChangeStatus, viewText: TextView) {
    when (state) {
        OrdersViewModel.StateChangeStatus.CONTENT -> {
            view.visibility = View.GONE
            viewText.visibility = View.GONE
        }

        OrdersViewModel.StateChangeStatus.CHANGE -> {
            view.visibility = View.VISIBLE
            viewText.visibility = View.GONE
        }

        else -> {
            view.visibility = View.VISIBLE
            viewText.visibility = View.VISIBLE
            viewText.text = OrdersViewModel.StateChangeStatus.ERROR.message
        }

    }
}

@BindingAdapter("setStatusChange")
fun steChangeStatusState(view: LinearLayout, state: OrdersViewModel.StateChangeStatus) {
    when (state) {
        OrdersViewModel.StateChangeStatus.CONTENT -> {
            view.visibility = View.VISIBLE
            view.alpha = 1f
            view.isClickable = true
        }
        OrdersViewModel.StateChangeStatus.CHANGE -> {
            view.visibility = View.VISIBLE
            view.alpha = 0.3f
            view.isClickable = false
        }
        else -> {
            view.visibility = View.VISIBLE
            view.alpha = 0.3f
            view.isClickable = false
        }
    }
}

@BindingAdapter("info:setType", "info:setStatus", "info:setListener", requireAll = true)
fun setStatus(view: LinearLayout, type: Boolean, status: String, onClickListener: View.OnClickListener) {
    view.removeAllViews()
    val statusCheck = Util.convertStatusStringInInt(status)
    val arrayStatus: List<Int> =
        when (statusCheck) {
            1 -> {
                emptyList()
            }
            2 -> {
                if (type || (!type && !AppSharedUser.serviceDelivery)) listOf(2, 4, 3) else listOf(2, 5, 4, 3)
            }
            3 -> {
                listOf(3)
            }
            4 -> {
                listOf(4)
            }
            else -> {
                listOf(5, 4, 3)
            }
        }

    if (arrayStatus.isNotEmpty()) {
        val arrayDrawable = listOf(
            R.drawable.selector_state_new,
            R.drawable.selector_state_inprogress,
            R.drawable.selector_state_cancel,
            R.drawable.selector_state_completed,
            R.drawable.selector_state_delivery
        )
        val arrayListStatus = ArrayList<Status>()
        arrayStatus.forEach {
            val statusObject =
                Status(it, view.context.resources.getStringArray(R.array.name_state)[it - 1], it == statusCheck)
            arrayListStatus.add(statusObject)
            val viewStatus = LayoutInflater.from(view.context).inflate(R.layout.status_item_view, null)
            viewStatus.tag = it
            val layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.weight = 1f
            viewStatus.layoutParams = layoutParams
            viewStatus.findViewById<CheckBox>(R.id.checkbox).buttonDrawable =
                    ContextCompat.getDrawable(
                        view.context,
                        arrayDrawable[it - 1]
                    )
            viewStatus.findViewById<CheckBox>(R.id.checkbox).isClickable = false
            viewStatus.findViewById<CheckBox>(R.id.checkbox).isChecked = it == statusCheck
            viewStatus.findViewById<TextView>(R.id.name_status).text =
                    view.context.resources.getStringArray(R.array.name_state)[it - 1]
            viewStatus.setOnClickListener(onClickListener)
            view.addView(viewStatus)
        }
    } else {

        val buttonApply = Button(view.context)
        val lParamsApply = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        lParamsApply.weight = 1f
        lParamsApply.leftMargin = 16
        lParamsApply.rightMargin = 8
        buttonApply.layoutParams = lParamsApply
        buttonApply.tag = 11
        buttonApply.backgroundResource = R.drawable.button_apply_order
        buttonApply.setTextColor(ContextCompat.getColor(view.context, R.color.color_text_button_apply))
        buttonApply.text = view.context.resources.getString(R.string.text_button_apply_order)
        buttonApply.textSize = 16f
        buttonApply.setOnClickListener(onClickListener)
        view.addView(buttonApply)

        val buttonCancel = Button(view.context)
        val lParamsCancel = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        lParamsCancel.weight = 1f
        lParamsCancel.leftMargin = 8
        lParamsCancel.rightMargin = 16
        buttonCancel.tag = 13
        buttonCancel.layoutParams = lParamsCancel
        buttonCancel.backgroundResource = R.drawable.button_cancel_order
        buttonCancel.setTextColor(ContextCompat.getColor(view.context, R.color.color_text_button_cancel))
        buttonCancel.text = view.context.resources.getString(R.string.text_button_cancel_order)
        buttonCancel.textSize = 16f
        buttonCancel.setOnClickListener(onClickListener)
        view.addView(buttonCancel)
    }

//    } else {
//        val statusCheck = Util.convertStatusStringInInt(status)
//        (0 until view.childCount).forEach {
//            val viewChild = view.getChildAt(it)
//            val checkbox = viewChild.findViewById<CheckBox>(R.id.checkbox)
//            if (checkbox.isChecked) checkbox.isChecked = false
//        }
//        val index = arrayStatus.indexOf(statusCheck)
//        val viewChecked = view.getChildAt(index)
//        viewChecked.findViewById<CheckBox>(R.id.checkbox).isChecked = true
//    }

}

@BindingAdapter("info:setInfoData", "info:setPermissionListener", requireAll = true)
fun setInfo(view: LinearLayout, order: Order, listener: View.OnClickListener) {
    if (view.childCount == 0) {
        val arrayTitle =
            view.context.resources.getStringArray(if (order.reservation) R.array.reservation_info_title else R.array.delivery_info_title)
        val arrayInfo = listOf(
            order.order_num,
            order.name,
            order.tel,
            view.context.getString(if (order.reservation) R.string.reservation else R.string.delivery),
            Util.convertFormatToDate(order.order_date),
            Util.convertFormatToTime(order.order_date),
            if (order.reservation) order.persons_quantity.toString() else order.address,
            if (order.reservation) order.table_num
                ?: "-" else "${order.change.toString()} ${order.restaurant.currency_symbol}",
            "\n${if (order.comment.isEmpty()) view.context.getString(R.string.empty_comment) else order.comment}"
        )


        arrayTitle.forEachIndexed { index, title ->
            val viewInfo = TextView(view.context)
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.bottomMargin = view.context.dip(8)
            viewInfo.layoutParams = layoutParams
            viewInfo.textSize = 15f
            viewInfo.text = Util.styledTextWithTitle(title, "${if (index == 2) "+" else ""}${arrayInfo[index]}")
            if (index == 1) viewInfo.setOnClickListener(listener)
            view.addView(viewInfo)
        }
    }
}

@BindingAdapter("info:setTimeDate")
fun setTextTimeDate(view: TextView, dateTime: String?) {
    view.text = if (dateTime != null)
        Util.convertTimeDate(dateTime)
    else
        "----"
}

@BindingAdapter("info:setTimeDateStatus")
fun setTextTimeDateStatus(view: TextView, order: Order?) {
    view.text = if (order != null)
        when (Util.convertStatusStringInInt(order.status)) {
            3 -> if (order.canceled_at != null) Util.convertTimeDate(order.canceled_at) else "----"
            4 -> if (order.completed_at != null) Util.convertTimeDate(order.completed_at) else "----"
            else -> "----"
        }
    else "----"
}


@BindingAdapter("statusRefresh")
fun setRefresh(view: SwipeRefreshLayout, state: Boolean) {
    view.isRefreshing = state
}

@BindingAdapter("setRefreshListener")
fun setRefreshListener(view: SwipeRefreshLayout, listener: SwipeRefreshLayout.OnRefreshListener) {
    view.setOnRefreshListener(listener)
}

@BindingAdapter("setMenuToolbar", "clickMenu", requireAll = true)
fun setMenu(view: Toolbar, resMenu: Int?, listener: Toolbar.OnMenuItemClickListener) {
    if (resMenu != null) {
        view.inflateMenu(resMenu)
        view.setOnMenuItemClickListener(listener)
    }
}

@BindingAdapter("setCoordinate")
fun setCoordinate(view: MapView, order: Order?) {
    if (order != null) {
        if (order.delivery) {
            view.visibility = View.VISIBLE
            view.getMapAsync { map ->

                val coordinateRestauranat = LatLng(order.restaurant.latitude, order.restaurant.longitude)
                val coordinateClient = LatLng(order.latitude, order.longitude)

                map.addMarker(MarkerOptions().position(coordinateRestauranat).title(order.restaurant_name))
                map.addMarker(MarkerOptions().position(coordinateClient).title(order.address))
//                val rest = SphericalUtil.computeDistanceBetween(coordinateClient, coordinateRestauranat)
//                val rest1 = SphericalUtil.computeDistanceBetween(coordinateRestauranat, coordinateClient)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinateClient, 15f))
//                val bounds = LatLngBounds(
//                    if (coordinateClient.latitude < coordinateRestauranat.latitude) coordinateClient else coordinateRestauranat,
//                    if (coordinateClient.latitude < coordinateRestauranat.latitude) coordinateRestauranat else coordinateClient
//                )
//                map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5))
            }
        } else {
            view.visibility = View.GONE
        }
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter("info:setTrash")
fun setListTrash(view: LinearLayout, order: Order?) {
    if (order != null && order.shop_cart.shop_cart_items.isNotEmpty()) {
        view.visibility = View.VISIBLE

        val viewInfo = TextView(view.context)
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.bottomMargin = view.context.dip(8)
        viewInfo.gravity = Gravity.START

        viewInfo.layoutParams = layoutParams
        viewInfo.textSize = 15f
        viewInfo.text = Util.styledTextWithTitle(view.context.getString(R.string.trash_in_order), "")
        view.addView(viewInfo)
        var summa = 0.0
        order.shop_cart.shop_cart_items.forEach { card ->
            summa += card.price * card.quantity
            val viewCard = TextView(view.context)
            val layoutParamsCard = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParamsCard.bottomMargin = view.context.dip(8)
            viewCard.layoutParams = layoutParamsCard
            viewCard.textSize = 13f
            viewCard.gravity = Gravity.END

            val string = StringBuilder()
                .append(card.name)
                .append(" --- ")
                .append(card.quantity)
                .append(" ")
                .append(view.context.getString(R.string.quantity))
                .toString()

            viewCard.text = string
            view.addView(viewCard)
        }

        val viewInfoAll = TextView(view.context)
        val layoutParamsAll = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParamsAll.bottomMargin = view.context.dip(8)
        viewInfoAll.gravity = Gravity.END
        viewInfoAll.layoutParams = layoutParamsAll
        viewInfoAll.textSize = 15f
        viewInfoAll.text = Util.styledTextWithTitle(
            view.context.getString(R.string.trash_in_order_cash),
            "$summa ${order.restaurant.currency_symbol}"
        )
        view.addView(viewInfoAll)


    } else
        view.visibility = View.GONE
}