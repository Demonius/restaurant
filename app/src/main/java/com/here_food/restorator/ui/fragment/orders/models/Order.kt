package com.here_food.restorator.ui.fragment.orders.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Order(
    val address: String,
    val cash: Boolean,
    val change: Double?,
    val comment: String,
    val created_at: String,
    val canceled_at:String?,
    val inprogress_at:String?,
    val completed_at:String?,
    val date: String,
    val delivery: Boolean,
    val delivery_cost: Double,
    val formatted_created_at: String,
    val formatted_date: String,
    val id: Long,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val order_date: String?,
    val order_num: String,
    val persons_quantity: Int,
    val reservation: Boolean,
    val restaurant: Restaurant,
    val restaurant_address: String,
    val restaurant_id: Int,
    val restaurant_name: String,
    val shop_cart: ShopCart,
    var status: String,
    val table_num: String?,
    val tel: String,
    val time: String,
    val updated_at: String,
    val user_id: Int
) : Parcelable {
    @Parcelize
    data class ShopCart(
        val created_at: String,
        val delivery: Boolean,
        val id: Long,
        val min_order: Int?,
        val reservation: Boolean,
        val restaurant_id: Int,
        val shop_cart_items: List<ShopCard>,
        val status: String,
        val updated_at: String,
        val user_id: Int
    ) : Parcelable {
        @Parcelize
        data class ShopCard(
            val created_at: String,
            val id: Int,
            val menu_item_id: Int,
            val name: String,
            val new_price: Double,
            val pic_url: String,
            val price: Double,
            val quantity: Int,
            val shop_cart_id: Int,
            val updated_at: String
        ) : Parcelable
    }

    @Parcelize
    data class Restaurant(
        val address_str: String,
        val brand: Brand,
        val delivery_service:Boolean,
        val cuisine_description: String,
        val currency_symbol: String,
        val delivery: Boolean,
        val delivery_min_order: Int,
        val id: Long,
        val info_text: String,
        val info_title: String,
        val juridical_info: String,
        val latitude: Double,
        val logo: Logo,
        val longitude: Double,
        val name: String,
        val photos: List<RestaurantPhoto>?,
        val price_category: String,
        val rating: Float?,
        val reservation: Boolean,
        val reservation_min_order: Int
    ) : Parcelable {

        @Parcelize
        data class RestaurantPhoto(
            val url: String,
            val thumb: Logo.Thumb
        ) : Parcelable

        @Parcelize
        data class Logo(
            val card: Card,
            val thumb: Thumb,
            val url: String
        ) : Parcelable {
            @Parcelize
            data class Thumb(
                val url: String
            ) : Parcelable

            @Parcelize
            data class Card(
                val url: String
            ) : Parcelable
        }

        @Parcelize
        data class Brand(
            val created_at: String,
            val id: Long,
            val name: String,
            val updated_at: String
        ) : Parcelable
    }

    fun getPersonQuantity(): String =
        persons_quantity.toString()
}