package com.here_food.restorator.ui.fragment.orders.view_model

import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import cn.refactor.multistatelayout.MultiStateLayout
import com.here_food.restorator.R
import com.here_food.restorator.RestoratorApplication
import com.here_food.restorator.api.ApiServiceContent
import com.here_food.restorator.api.request.RequestChangeStatus
import com.here_food.restorator.core.BaseRecyclerViewAdapter
import com.here_food.restorator.ui.fragment.orders.InterfaceFragment
import com.here_food.restorator.ui.fragment.orders.InterfaceFragmentState
import com.here_food.restorator.ui.fragment.orders.OrderFragment
import com.here_food.restorator.ui.fragment.orders.OrdersFragment
import com.here_food.restorator.ui.fragment.orders.adapter.AdapterViewOrders
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.util.AppSharedUser
import com.here_food.restorator.util.Util
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast

class OrdersViewModel(private val fragment: Fragment) : InterfaceFragmentState {
    override fun stateChange(order: Order) {
        this@OrdersViewModel.order.set(order)
        fragment.context?.toast("update")
    }

    val title = ObservableField<String>()
    val statusRefresh = ObservableField<Boolean>()
    val orders = ObservableArrayList<Order>()
    val adapter = ObservableField<BaseRecyclerViewAdapter<Order>>()
    val stateLayout = ObservableField<Int>()
    val order = ObservableField<Order>()
    val orderNew = ObservableField<Order>()

    val menu = ObservableField<Int>()

    val stateChangeStatus = ObservableField<StateChangeStatus>(StateChangeStatus.CONTENT)

    private var stateStatus: Int = 0


    fun loadTabs() {
        title.set(AppSharedUser.restaurantName)
    }

    fun loadOrders(state: Int) {
        adapter.set(AdapterViewOrders())
        getOrders(state)
        stateStatus = state
    }

    fun onRefreshListener() {
        orders.clear()
        getOrders(stateStatus)
    }

    fun onMenuClick(item:MenuItem) :Boolean{
        return when(item.itemId){
            R.id.exit -> {
                if (fragment is OrdersFragment)
                fragment.logout()

                true
            }
            else ->{
                false
            }
        }
    }

    private fun getOrders(state: Int) {
        statusRefresh.set(true)
        ApiServiceContent.getOrders(state) { observable ->
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    statusRefresh.set(false)
                    when (it.code()) {
                        200 -> {
                            val ordersLoad = it.body()!!.data
                            if (ordersLoad == null || ordersLoad.isEmpty())
                                stateLayout.set(MultiStateLayout.State.EMPTY)
                            else {

                                this@OrdersViewModel.orders.addAll(ordersLoad)
                                stateLayout.set(MultiStateLayout.State.CONTENT)
                            }
                        }
                        else -> {
                            stateLayout.set(MultiStateLayout.State.ERROR)
                            Log.e(Util.LOG_TAG, "code answer => ${it.code()}")

                        }
                    }
                }, {
                    statusRefresh.set(false)
                    stateLayout.set(MultiStateLayout.State.ERROR)
                    Log.e(Util.LOG_TAG, "error load list => $it")
                    it.stackTrace.forEach {
                        Log.e(Util.LOG_TAG, "line=> ${it.lineNumber} -> error ====>>>> $it")
                    }
                })
        }
    }

    fun onClickOrder(item: Any) {
        if (item is Order) {
            val orderFragment = OrderFragment.newInstance(item)
            orderFragment.registrationInterface(object : InterfaceFragment {
                override fun updateStatus(orderId: Long, statusNew: String) {
                }

            })
            orderFragment.show(fragment.childFragmentManager, "dialogOrder")
        }
    }

    fun onClickBack(view: View) {
        when (view) {

            is Button -> {
                val order = this.order.get()
                when (view.tag as Int) {
                    11 -> {
                        setStatusOnServer(order!!.id, 2)
                    }
                    13 -> {
                        setStatusOnServer(order!!.id, 3)
                    }
                }
            }

            is TextView -> {
                val tel = view.text
                (fragment as OrderFragment).callClient(tel.substring(1))
            }

            is AppCompatImageButton -> if (fragment is OrderFragment)
                fragment.dismiss()

            is ConstraintLayout -> {
                view.isClickable = false
                val tag = view.tag as Int
                val order = this.order.get()
                val statusOrder = Util.convertStatusStringInInt(order!!.status)
                when (statusOrder) {
                    1 -> {
                        if (tag !in listOf(2, 3, 4, 5)) return
                    }
                    2 -> {
                        if (tag !in listOf(3, 4, 5)) return
                    }
                    3 -> {
                        return
                    }
                    4 -> {
                        return
                    }
                    5 -> {
                        if (tag !in listOf(3, 4)) return
                    }
                }

                setStatusOnServer(order.id, tag)
            }
        }
    }

    private fun setStatusOnServer(idOrder: Long, status: Int) {
        stateChangeStatus.set(StateChangeStatus.CHANGE)
        ApiServiceContent.changeStatus(RequestChangeStatus(idOrder, status)) { observable ->
            observable.subscribeOn(Schedulers.io())

                .doOnError {
                    stateChangeStatus.set(StateChangeStatus.ERROR)
                    Log.e(Util.LOG_TAG, "error change status => $it")
                }
                .filter {
                    when (it.code()) {
                        200 -> true
                        404 -> {
                            stateChangeStatus.set(StateChangeStatus.ERROR)
                            false
                        }
                        400 -> true

                        else -> {
                            stateChangeStatus.set(StateChangeStatus.ERROR)
                            false
                        }
                    }
                }
                .flatMap { ApiServiceContent.getOrder(idOrder) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response ->
                    when (response.code()) {
                        200 -> {
                            stateChangeStatus.set(StateChangeStatus.CONTENT)
                            val orderNew = response.body()!!.data
                            if (fragment is OrderFragment) fragment.updateOrder(orderNew)
                        }
                    }
                }
        }
    }


    fun deleteOrder(order: Order) {
        if (this.orders.size == 1) stateLayout.set(MultiStateLayout.State.EMPTY)
//        val arrayList = ArrayList<Order>()
//        arrayList.addAll(orders)
//        arrayList.remove(order)
//        orders.clear()
//        orders.addAll(arrayList)
        val orderRemove = orders.find { it.id == order.id }
        orders.remove(orderRemove)
    }

    fun addOrder(order: Order) {
        if (stateLayout.get() == MultiStateLayout.State.EMPTY) stateLayout.set(MultiStateLayout.State.CONTENT)
        orders.add(0, order)
    }

    enum class StateChangeStatus(val message: String) {
        CONTENT(""),
        CHANGE(""),
        ERROR(RestoratorApplication.instance.resources.getString(R.string.error_change_order_status))
    }

}