package com.here_food.restorator.ui.main

import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.here_food.restorator.R
import com.here_food.restorator.api.ApiServiceContent
import com.here_food.restorator.api.request.RequestChangeStatus
import com.here_food.restorator.databinding.ActivityMainBinding
import com.here_food.restorator.service.ServiceWebSocket
import com.here_food.restorator.ui.fragment.orders.InterfaceFragment
import com.here_food.restorator.ui.fragment.orders.OrderFragment
import com.here_food.restorator.ui.fragment.orders.OrdersFragment
import com.here_food.restorator.ui.fragment.orders.StateOrdersFragment
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.ui.main.view_model.MainViewModel
import com.here_food.restorator.util.StoredPreferencesUser
import com.here_food.restorator.util.StringPreference
import com.here_food.restorator.util.Util
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.displayMetrics
import org.jetbrains.anko.toast
import java.lang.IllegalStateException
import java.util.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val NOTIFICATION_ID = "notify_id"
        const val STATUS_ID = "status_id"
        const val ORDER = "orderMax"
        const val OPEN = "open"
    }

    private lateinit var bindingMain: ActivityMainBinding
    private val CODE_SERVICE = 50
    private var orderMax: Order? = null
    var locale by StringPreference(StoredPreferencesUser.Type.Locale)

    override fun onNewIntent(intent: Intent?) {
        if (intent != null) {
//            val order = intent.extras.getParcelable<Order>(ORDER)
            if (intent.getBooleanExtra(OPEN, false)) {

                openOrderInFragment(intent.getParcelableExtra(ORDER))

            } else {

                val orderId = intent.getLongExtra(NOTIFICATION_ID, -1)
                val status_id = intent.getIntExtra(STATUS_ID, -1)


                if (orderId != -1L && status_id != -1)

                    ApiServiceContent.changeStatus(RequestChangeStatus(orderId, status_id)) { observable ->
                        observable.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { response ->
                                if (response.code() == 200) {

                                }
                                //TODO else structure and throwable
                            }
                    }
            }
        }
        super.onNewIntent(intent)
    }

    private fun openOrderInFragment(order: Order?) {
        var fragment: Fragment? = null
        supportFragmentManager.fragments.forEach {
            if (it is OrdersFragment) fragment = it
        }
        if (fragment != null) {
            var fragmentState: Fragment? = null
            fragment!!.childFragmentManager.fragments.forEach {
                if (it is StateOrdersFragment)
                    if (Util.convertStatusStringInInt(order!!.status) == it.arguments!!.getInt(StateOrdersFragment.STATE))
                        fragmentState = it
            }
            if (fragmentState != null)
                try {
                    (fragmentState as StateOrdersFragment).openOrder(order!!)
                } catch (error: IllegalStateException) {
                    (fragmentState as StateOrdersFragment).onResume()
                    val intent = Intent(StateOrdersFragment.BROADCAST_ACTION).apply {
                        putExtra(StateOrdersFragment.OPEN, true)
                        putExtra(StateOrdersFragment.ORDER_DATA, order)
                    }
                    sendBroadcast(intent)
                }
        }
    }

    override fun onResume() {
        super.onResume()
        if (orderMax != null) {
            bindingMain.mainView?.setOrdersFragment()
            bindingMain.mainView?.order?.set(orderMax)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val arrayLocale = listOf("ru", "tr", "en")

        val localeNow = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            resources.configuration.locales[0].language
        else
            resources.configuration.locale.language

        locale = if (localeNow in arrayLocale) localeNow else "en"

        val localeSet = Locale(locale)
        Locale.setDefault(localeSet)
        val res = resources
        val configuration = Configuration(res.configuration)
        configuration.locale = localeSet
        res.updateConfiguration(configuration, displayMetrics)


        bindingMain = DataBindingUtil.setContentView(this, R.layout.activity_main)
        bindingMain.mainView = MainViewModel(this)

        if (intent != null) {
            val order = intent.getParcelableExtra<Order>(ORDER)
            if (intent.getBooleanExtra(OPEN, false)) {
                bindingMain.mainView?.order?.set(order)
            } else {


                val orderId = intent.getLongExtra(NOTIFICATION_ID, -1)
                val status_id = intent.getIntExtra(STATUS_ID, -1)


                if (orderId != -1L && status_id != -1)

                    ApiServiceContent.changeStatus(RequestChangeStatus(orderId, status_id)) { observable ->
                        observable.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { response ->
                                if (response.code() == 200) {
                                }
                                //TODO else structure and throwable
                            }
                    }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (supportFragmentManager.fragments.isNotEmpty())
            supportFragmentManager.fragments.forEach {
                it.onRequestPermissionsResult(
                    requestCode,
                    permissions,
                    grantResults
                )
            }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    fun connectToWebServer() {
        val intent = Intent(this, ServiceWebSocket::class.java)
        val pendingIntent = createPendingResult(CODE_SERVICE, intent, 0)
        intent.putExtra("pendingIntent", pendingIntent)
        startService(intent)
    }

    private fun showMessage(message: String) {
        toast(message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 555) {
            val status = data!!.getIntExtra("status", 0)

            when (status) {
                1 -> showMessage(data.getStringExtra("message"))
            }

        }
    }

    fun closeConnectionWebSocket() {
        val intent = Intent(this, ServiceWebSocket::class.java)
        stopService(intent)
    }
}
