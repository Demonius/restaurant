package com.here_food.restorator.ui.main.binding

import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.here_food.restorator.ui.fragment.orders.OrdersFragment
import com.here_food.restorator.ui.fragment.orders.models.Order

//@BindingAdapter("setFragment", "setActivity", requireAll = true)
//fun setFragment(view: FrameLayout, fragment: Fragment, activity: AppCompatActivity) {
//    if (order != null && fragment is OrdersFragment) {
//        val bundle = Bundle()
//        bundle.putParcelable(OrdersFragment.ORDER, order)
//        fragment.arguments = bundle
//    }
//    activity.supportFragmentManager.beginTransaction()
//        .replace(view.id, fragment)
//        .commit()
//}

@BindingAdapter("setOrder", "setFragment", "setActivity", requireAll = true)
fun setOpenOrder(view: FrameLayout, order: Order?, fragment: Fragment, activity: AppCompatActivity) {
    if (order != null && fragment is OrdersFragment) {
        val bundle = Bundle()
        bundle.putParcelable(OrdersFragment.ORDER, order)
        fragment.arguments = bundle
        activity.supportFragmentManager.beginTransaction()
            .replace(view.id, fragment)
            .commit()
    } else
        activity.supportFragmentManager.beginTransaction()
            .replace(view.id, fragment)
            .commit()

}