package com.here_food.restorator.ui.fragment.orders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import cn.refactor.multistatelayout.MultiStateLayout
import com.here_food.restorator.R
import com.here_food.restorator.databinding.StateOrdersFragmentBinding
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.ui.fragment.orders.view_model.OrdersViewModel
import com.here_food.restorator.util.Util
import kotlinx.android.synthetic.main.state_orders_fragment.*
import org.jetbrains.anko.sdk27.coroutines.onClick


class StateOrdersFragment : Fragment() {


    companion object {
        const val STATE = "state"
        private const val ORDER_OPEN = "order_open"
        fun newInstance(state: Int, order: Order?): StateOrdersFragment {
            val fragment = StateOrdersFragment()
            val bundle = Bundle()
            bundle.putInt(STATE, state)
            bundle.putParcelable(ORDER_OPEN, order)
            fragment.arguments = bundle
            return fragment
        }

        const val BROADCAST_ACTION = "com.here_food.restorator.order"
        const val STATUS = "status"
        const val ORDER = "order_id"
        const val DATA = "data"
        const val ORDER_DATA = "order"
        const val STATUS_NEW = "new_status"
        const val STATUS_OLD = "old_status"
        const val OPEN = "open_order"
    }

    private lateinit var interfaceChange: InterfaceFragmentState
    private lateinit var binding: StateOrdersFragmentBinding
    private var state = 0
    private val intentFilter = IntentFilter(BROADCAST_ACTION)
    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                if (intent.getBooleanExtra(OPEN, false)) {
                    val order = intent.getParcelableExtra<Order>(ORDER_DATA)
                    binding.ordersView?.onClickOrder(order)

                } else if (intent.getBooleanExtra(DATA, false)) {

                    val newStatus = Util.convertStatusStringInInt(intent.getStringExtra(STATUS_NEW))
                    val oldStatus = Util.convertStatusStringInInt(intent.getStringExtra(STATUS_OLD))
                    when (state) {
                        oldStatus -> {
                            val order = intent.getParcelableExtra<Order>(ORDER_DATA)
//                            binding.ordersView!!.order.set(order)
//                            binding.ordersView?.orders?.remove(order)
//                            binding.ordersView?.order?.set(order)
                            binding.ordersView?.deleteOrder(order)
                        }
                        newStatus -> {
                            val order = intent.getParcelableExtra<Order>(ORDER_DATA)
//                            binding.ordersView!!.orderNew.set(order)
                            binding.ordersView?.addOrder(order)
                        }
                    }

//                    binding.ordersView = OrdersViewModel(this@StateOrdersFragment)
//                    binding.ordersView?.loadOrders(state)
                } else {
                    if (state == 1) {
                        val order = intent.getParcelableExtra<Order>(ORDER_DATA)
//                        binding.ordersView!!.orderNew.set(order)
                        binding.ordersView?.addOrder(order)
                    }

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(broadcastReceiver)
    }

    fun registrationInterface(interfaceChange: InterfaceFragmentState) {
        this.interfaceChange = interfaceChange
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.state_orders_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        state = arguments!!.getInt(STATE)
        val order = arguments?.getParcelable<Order>(ORDER_OPEN)

        binding.ordersView = OrdersViewModel(this)
        binding.ordersView?.loadOrders(state)
        if (order != null)
            binding.ordersView?.onClickOrder(order)
        stateLayout.setOnStateViewCreatedListener { viewLayout, stateLayout ->
            when (stateLayout) {
                MultiStateLayout.State.EMPTY -> {
                    viewLayout.findViewById<TextView>(R.id.textInfoNoElement).onClick {
                        binding.ordersView?.onRefreshListener()
                    }
                }

                MultiStateLayout.State.ERROR -> {
                    viewLayout.findViewById<Button>(R.id.reloadData).onClick {
                        binding.ordersView?.onRefreshListener()
                    }
                }
            }
        }
        refreshLayout.setOnRefreshListener {
            binding.ordersView?.onRefreshListener()
        }
    }

    fun openOrder(order: Order) {
        binding.ordersView?.onClickOrder(order)
    }
//    fun updateStateOrder(order: Order) {
//        interfaceChange.stateChange(order)
//    }
//
//    fun updateOrder() {
//        binding.ordersView?.loadOrders(state)
//    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (childFragmentManager.fragments.isNotEmpty())
            childFragmentManager.fragments.forEach {
                it.onRequestPermissionsResult(
                    requestCode,
                    permissions,
                    grantResults
                )
            }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }
}

interface InterfaceFragmentState {
    fun stateChange(order: Order)
}