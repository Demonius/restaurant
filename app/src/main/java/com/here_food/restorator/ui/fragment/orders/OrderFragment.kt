package com.here_food.restorator.ui.fragment.orders

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.google.android.gms.maps.MapView
import com.here_food.restorator.R
import com.here_food.restorator.databinding.OrderFragmentBinding
import com.here_food.restorator.ui.fragment.orders.models.Order
import com.here_food.restorator.ui.fragment.orders.view_model.OrdersViewModel
import com.here_food.restorator.util.Util
import kotlinx.android.synthetic.main.order_fragment.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton
import java.io.Serializable


class OrderFragment : DialogFragment() {


    companion object {
        private const val ORDER = "order"
        fun newInstance(order: Order): OrderFragment {
            val fragment = OrderFragment()
            val bundle = Bundle()
            bundle.putParcelable(ORDER, order)
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var binding: OrderFragmentBinding
    private lateinit var interfaceFragment: InterfaceFragment
    private lateinit var telString: String

    fun registrationInterface(interfaceFragment: InterfaceFragment) {
        this.interfaceFragment = interfaceFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_NoActionBar)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.order_fragment, container, false)
        val view = binding.root
        view.findViewById<MapView>(R.id.map_set).onCreate(savedInstanceState)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val order = arguments!!.getParcelable(ORDER) as Order
        binding.orderView = OrdersViewModel(this)
        binding.orderView?.order?.set(order)
        binding.order = order

    }


    fun updateOrder(order: Order?) {
        binding.orderView?.order?.set(order)
        binding.order = order
        interfaceFragment.updateStatus(order!!.id, order.status)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 100) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                calling(telString)
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun callClient(tel: String) {

        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            telString = tel
            val alert = alert(R.string.permission_for_call) {
                yesButton {
                    ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CALL_PHONE), 100)
                }
                noButton {

                }
            }
            alert.isCancelable = false
            alert.show()

        } else
            calling(tel)

    }

    private fun calling(tel: String) {
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$tel")
        activity!!.startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        map_set.onResume()

    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            map_set.onDestroy()
        } catch (error: NullPointerException) {
            Log.e(Util.LOG_TAG, "error mapview destroy ignor")
        }
    }

    override fun onPause() {
        super.onPause()
        map_set.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_set.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (map_set != null) {
            val bundle = Bundle()
            bundle.putFloat("one", map_set.cameraDistance)
            map_set.onSaveInstanceState(bundle)
        }
    }

}

interface InterfaceFragment {
    fun updateStatus(orderId: Long, statusNew: String)
}