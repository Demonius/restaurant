package com.here_food.restorator.ui.fragment.orders.binding

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import com.here_food.restorator.core.BaseRecyclerViewAdapter

@Suppress("UNCHECKED_CAST")
@BindingAdapter(value = ["items"], requireAll = false)
fun <T> bindAdapterItems(recyclerView: RecyclerView, list: ObservableList<T>) {
    val viewModelAdapter = (recyclerView.adapter as? BaseRecyclerViewAdapter<T>) ?: return
    viewModelAdapter.bind(list.toList<T>())
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("item")
fun <T> deleteItem(recyclerView: RecyclerView, item: ObservableField<T>) {
    val viewModelAdapter = (recyclerView.adapter as? BaseRecyclerViewAdapter<T>) ?: return
    if (item.get() == null) return
    viewModelAdapter.delete(item.get()!!)
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("itemNew")
fun <T> newItem(recyclerView: RecyclerView, item: ObservableField<T>) {
    val viewModelAdapter = (recyclerView.adapter as? BaseRecyclerViewAdapter<T>) ?: return
    if (item.get() == null) return
    viewModelAdapter.add(item.get()!!)
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("itemClick")
fun <T> recyclerItemClick(recyclerView: RecyclerView, clickable: BaseRecyclerViewAdapter.OnItemClickListener<T>) {
    val viewModelAdapter = (recyclerView.adapter as? BaseRecyclerViewAdapter<T>) ?: return
    viewModelAdapter.setOnItemClickListener(clickable)
}