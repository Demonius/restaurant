package com.here_food.restorator.core


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView


abstract class BaseRecyclerViewAdapter<T>(@LayoutRes private val layoutId: Int, private val bindingId: Int) :
    RecyclerView.Adapter<BaseRecyclerViewAdapter.ViewHolder>() {
    private var items = ArrayList<T>()
    private var onItemClickListener: OnItemClickListener<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.setVariable(bindingId, items[position])
        holder.binding?.executePendingBindings()
        holder.binding?.root?.setOnClickListener {
            onItemClickListener?.onClick(items[position])
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener<T>) {
        onItemClickListener = listener
    }

    fun bind(itemsNew: List<T>) {

        if (itemsNew.isEmpty()) {
            this.items.clear()
            notifyDataSetChanged()
        } else {
            if (this.items.isEmpty()) {
                val positionStart = this.items.size
                this.items.addAll(itemsNew)
                notifyItemRangeInserted(positionStart, itemsNew.size)
            } else {
//                val itemsNow = this.items
                val iteratorItems = this.items.iterator()
                while (iteratorItems.hasNext()){
                    val itemOne = iteratorItems.next()
                    if (itemOne !in itemsNew) {
                        val index = this.items.indexOf(itemOne)
                        iteratorItems.remove()
                        notifyItemRemoved(index)
                    }
                }
                itemsNew.forEachIndexed { index, item ->
                    if (item !in this.items) {
                        this.items.add(index, item)
                        notifyItemInserted(index)
                    }
                }


            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = DataBindingUtil.bind<ViewDataBinding>(view)
    }

    interface OnItemClickListener<in T> {
        fun onClick(item: T)
    }

    fun getCurrentItems(): List<T> = items

    fun delete(item: T) {
        val index = this.items.indexOf(item)
        this.items.remove(item)
        notifyItemRemoved(index)
    }

    fun add(item: T) {
        this.items.add(0, item)
        notifyItemInserted(0)
    }

}