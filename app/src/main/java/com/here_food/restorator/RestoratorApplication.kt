package com.here_food.restorator

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import androidx.multidex.MultiDexApplication
import cn.refactor.multistatelayout.MultiStateConfiguration
import cn.refactor.multistatelayout.MultiStateLayout
import com.google.android.gms.security.ProviderInstaller
import com.here_food.restorator.util.SecureText

class RestoratorApplication : MultiDexApplication() {
    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var instance: Application
        lateinit var secureText: SecureText
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        secureText = SecureText(this)

        val builder = MultiStateConfiguration.Builder()
        builder.commonLoadingLayout = R.layout.layout_state_loading
        builder.commonErrorLayout = R.layout.layout_state_error
        builder.commonEmptyLayout = R.layout.layout_state_empty
        builder.animDuration = 500
        builder.isAnimEnable = true
        MultiStateLayout.setConfiguration(builder)


        ProviderInstaller.installIfNeededAsync(this, object : ProviderInstaller.ProviderInstallListener {
            override fun onProviderInstallFailed(p0: Int, p1: Intent?) {
            }

            override fun onProviderInstalled() {
            }
        })
    }
}